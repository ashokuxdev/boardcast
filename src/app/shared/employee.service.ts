import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor() { }

  form: FormGroup = new FormGroup({
    $key: new FormControl(null),
    fullName: new FormControl('', Validators.required),
    mobile: new FormControl('', [Validators.required, Validators.minLength(10)]),
    email: new FormControl('', Validators.email),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    gender: new FormControl('1'),
    pincode: new FormControl('', Validators.required),
    city: new FormControl(''),
    state: new FormControl(''),
    country: new FormControl(0),
    interest: new FormControl(0, Validators.required),
    //hireDate: new FormControl(''),
    iAgree: new FormControl(false, Validators.required)
  });

  initializeFormGroup() {
    this.form.setValue({
      $key: null,
      fullName: '',
      mobile: '',
      email: '',
      password: '',
      gender: '1',
      pincode: '',
      city: '',
      state: '',
      country: '0',
      interest: '0',
      //hireDate: '',
      iAgree: false
    });
  }
}
